import fire
from bwah.utils import cmd_runner

if __name__ == '__main__':
    fire.Fire(cmd_runner.CommandLineRunner)
