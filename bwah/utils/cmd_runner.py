class CommandLineRunner:
    """
    CommandLineRunnerClass defines the main commands that can be run from the command line
    """
    def train(self):
        ...
    def predict(self, a: str):
        """
        Uses the input parameters for predicting
        
        :param a: A parameter

        """
        print("hello world")