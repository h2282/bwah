Usage
=====

.. _instalation:

Installation
------------

To use bwah, first install it using pip:

.. code-block:: console

   (.venv) $ pip install bwah

Command-line usage
------------------

Once bwah is installed, it can be used on the command line for both training and inference of intents and entities. 
In order to do so, you just need to provide a configuration file, which is a YAML file containing the following parameters:

.. code-block:: yaml

   # configuration file
   intent_classifier:
       model_name: "bert"
       model_path: "path